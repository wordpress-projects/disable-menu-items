=== Silencesoft Disable Menu Items ===
by: Byron Herrera - bh at silencesoft dot co
Contributors: silence
Donate link: http://www.silencesoft.co/
Tags: menu, disable, hide, menu items
Requires at least: 3.0.1
Tested up to: 3.5
Stable tag: 1.0
License: WTFPL
License URI: http://www.wtfpl.net/txt/copying/

This plugin allow to hide or show menu items,

== Description ==

This plugin allows to disable menu items and hide them from menu without
the need to delete them.

This plugin use phpQuery 0.9.5 from Tobiasz Cudnik
 * phpQuery is a server-side, chainable, CSS3 selector driven
 * Document Object Model (DOM) API based on jQuery JavaScript Library.
http://code.google.com/p/phpquery/

== Installation ==

Upload plugin folder to plugins folder.
Enable plugin.
Go to appearance menu options.
In every menu item will be a new option "Disable", use it.

== Screenshots ==

1. Mofifying a menu item

== Changelog ==

= 1.0 =
* First release.

